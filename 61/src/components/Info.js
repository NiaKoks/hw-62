import React, {Component} from 'react';

class Info extends Component {
    render() {
        return (
            <div>

                <div >
                    <h3 className='country-name'>{this.props.CountryName}</h3>
                    <b className='capital-name'>{this.props.Capital}</b>
                    <p> {this.props.PplLive}</p>
                    <img className='flag' src={this.props.Flag} alt={this.props.CountryName}/>
                    {
                        this.props.BorderCon.map((country, index) => {
                            return(<p className='border' key = {index}>{country}</p>)
                        })
                    }
                </div>
            </div>
        );
    }
}

export default Info;