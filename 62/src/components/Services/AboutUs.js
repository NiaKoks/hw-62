import React, {Component} from 'react';
import Nav from "./Nav"
import "./css/aboutus.css"
import {NavLink} from "react-router-dom";
class AboutUs extends Component {
    render() {
        return (
            <div className="about-us-page">
                <Nav></Nav>
                <p className="about-us">Our company is creating unique patterns depends on your needs.</p>
                <p> Our customers service is working 24/7 and ready to gave you a hand when there is need.</p>
                <h3>Remember: Patterns for all situations</h3>
                <div className="card card-1">
                    <h4>Gallery</h4>
                    <img src="http://www.earth-community.org/img/2018/06/years-styles-patterns-per-outlet-apartments-budget-entry-home-bedroom-year-internships-creative-design-timeline-interiors-designs-and-top-textures-mac-ideas-interior-salary-example.jpg" alt=""/>
                    <button className="more-btn"><NavLink to="/notready">more</NavLink></button>
                </div>
                <div className="card card-2">
                    <h4>PRO tips</h4>
                    <img src="https://thecasacollective.com/wp-content/uploads/2018/07/MixingPatterns_000.jpg" alt=""/>
                    <button className="more-btn"><NavLink to="/notready">more</NavLink></button>
                </div>
                <div className="card card-3">
                    <h4>Contact with us</h4>
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTW5MxBBIk02yX3wDx6hSbLk9yPhdp9YR2kRIuFUndkTFJ-NA37Lg" alt=""/>
                    <button className="more-btn"><NavLink to="/contact">more</NavLink></button>
                </div>
            </div>
        );
    }
}

export default AboutUs;