import React, {Component} from 'react';
import {NavLink} from "react-router-dom";

class Nav extends Component {
    render() {
        return (
            <div>
                <NavLink to="/">Home</NavLink>
                <NavLink to="/contact">Contacts</NavLink>
                <NavLink to="/aboutus">About Us</NavLink>
            </div>
        );
    }
}

export default Nav;