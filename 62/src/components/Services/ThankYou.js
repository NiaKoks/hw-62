import React, {Component} from 'react';
import {NavLink} from "react-router-dom";

class ThankYou extends Component {
    render() {
        return (
            <div className="tnx-page">
                <h3>Thank you for sharing your ideas with us!</h3>
                <h4>You make us better!</h4>
                <img className="tnx" src="https://vignette.wikia.nocookie.net/koror-blog-org/images/9/91/Bowing-down.gif/revision/latest?cb=20160731020536" alt="hugging bears"/>
                <button className="back-to-home-btn"><NavLink to="/">Back to Main page</NavLink></button>
            </div>
        );
    }
}

export default ThankYou;