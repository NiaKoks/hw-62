import React, {Component} from 'react';
import Nav from "./Services/Nav"

class Home extends Component {
    render() {
        return (
            <div className="home-page">
                <Nav></Nav>
                <h2 className="home-h2">Patterns for LIFE</h2>

                <p className="home-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Eius expedita maxime, mollitia nobis perspiciatis quisquam tempore ut veritatis!
                    Accusantium debitis dolorem id ipsam maxime molestiae nisi, optio provident sunt veniam!</p>
                <div className="ex-pics">
                    <img className=" ex ex-pic-1" src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/ed-best-decor-david-kaihoi-1525976827.jpg"></img>
                    <img className="ex ex-pic-2" src="https://cdn.homedit.com/wp-content/uploads/2015/07/honeycomb-shelves.jpg"></img>
                </div>
            </div>
        );
    }
}

export default Home;