import React, { Component } from 'react';
import './App.css';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import ContactInfo from "./components/Services/ContactInfo";
import AboutUs from "./components/Services/AboutUs";
import Home from "./components/Home";
import ThankYou from "./components/Services/ThankYou";
import NotReady from "./components/Services/NotReady"

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Home}></Route>
          <Route path="/thankyou" component={ThankYou}></Route>
          <Route path="/contact" component={ContactInfo}></Route>
          <Route path="/aboutus" component={AboutUs}/>
          <Route path="/notready" component={NotReady}/>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
